#!/usr/bin/env ruby
# frozen_string_literal: true

require 'tty-config'

$config = TTY::Config.new

$config.append_path(APP_ROOT)

$config.read
