#!/usr/bin/env ruby
# frozen_string_literal: true

require "ruby-spacy"


module KBGPT
  class Classifier
    def initialize
      @nlp = Spacy::Language.new("en_core_web_lg")
    end
  end
end


locations = $config.fetch(:document)["locations"]
sources = locations.map { |l| Pathname.new(l) }
import = KBGPT::Import.new(sources)
import.files
import.files.first
file = PDF2Text.new(import.files.first.realdirpath.to_s)
file.extract

nlp = Spacy::Language.new("en_core_web_lg")

x = nlp.read(file.text_data)
