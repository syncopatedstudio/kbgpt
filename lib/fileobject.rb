#!/usr/bin/env ruby
# frozen_string_literal: true

require "poppler"
require "parallel"
require "pragmatic_tokenizer"
require "tiktoken_ruby"

module KBGPT

  class FileObject
    attr_reader :path, :name, :type
    attr_accessor :full_text

    def initialize(path)
      @path = Pathname.new(path)
      @name = @path.basename.to_s
      @type = @path.extname
      @full_text = []
    end

    def read
      # Implement the logic to read the file here
      # You can use File.read or other methods to read the contents of the file
    end

    def write(content)
      # Implement the logic to write content to the file here
      # You can use File.write or other methods to write content to the file
    end
  end

end

class PDF2Text
  include Logging

  THREADS = 4

  attr_reader :file_path, :text_data

  # Initializes the PDF2Text class
  def initialize(file_path)
    @file_path = file_path
    @file_name = File.basename(file_path)
    @text_data = ""
  end

  # Extracts the text from the PDF file
  def extract
    doc = Poppler::Document.new(@file_path)
    @text_data = ""

    Parallel.each(0...doc.n_pages, in_threads: THREADS) do |page_num|
      page = doc.get_page(page_num)
      @text_data += "#{page.get_text}\n"
    end

    logger.debug("text_data: #{@text_data}")

    @text_data

  end

  # Splits the text into chunks of `max_tokens` tokens
  def split_text(max_tokens = 800)
    encoder = Tiktoken.get_encoding("cl100k_base")
    lines = @text_data.split("\n")

    logger.debug("lines: #{lines}")

    # lines.each do |

    split_texts = []
    current_text = ""
    current_tokens = 0

    Parallel.each(lines, in_threads: THREADS) do |line|
      line_tokens = encoder.encode(line)

      logger.debug("line_tokens: #{line_tokens}")

      line_token_count = line_tokens.size

      if current_tokens + line_token_count > max_tokens
        split_texts << { "text" => current_text.strip, "tokens" => current_tokens }
        current_text = ""
        current_tokens = 0
      end

      current_text += "#{line}\n"
      current_tokens += line_token_count
    end

    # Add the remaining text if it's not empty

    #TODO: add more metadata

    split_texts << { "text" => current_text.strip, "tokens" => current_tokens } unless current_text.strip.empty?

    #####************
    logger.debug("split_texts: #{split_texts}")
    #####************

    split_texts
  end
end
