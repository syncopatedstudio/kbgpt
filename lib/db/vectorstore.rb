##!/usr/bin/env ruby
# frozen_string_literal: true

require "chroma-db"

module KBGPT

  class VectorStore
    include Logging

    attr_accessor :chroma

    def initialize(index)
      @chroma = Langchain::Vectorsearch::Chroma.new(
        url: $config.fetch(:db)["chromadb"],
        index_name: index,
        llm: Langchain::LLM::OpenAI.new(api_key: ENV["OPENAI_API_KEY"])
      )
      @chroma.create_default_schema
    end

    def embed(words)
      @chroma.embed(text: words)
    end


  end

end

# vstore = KBGPT::VectorStore.new("Ambisonics")

# vstore.embed(words)
