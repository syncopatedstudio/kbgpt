#!/usr/bin/env ruby
# frozen_string_literal: true

# Import necessary libraries
require 'tomoto'

# Load document data
documents = [
  "This is the first document",
  "This document is the second document",
  "And this is the third one",
  "Is this the first document?"
]

# Preprocess document data
tokenizer = PragmaticTokenizer::Tokenizer.new(language: "en")
tokenized_documents = documents.map { |doc| tokenizer.tokenize(doc) }

# Create LDA model
model = Tomoto::LDA.new

# Set model parameters
model.k = 3 # Number of topics
model.alpha = 0.1 # Dirichlet prior for document-topic distribution
model.eta = 0.01 # Dirichlet prior for topic-word distribution

# Train the model
tokenized_documents.each { |doc| model.add_doc(doc) }
model.train(100) # Number of iterations

# Retrieve learned topics and their associated words
topics = model.summary(num_words: 5)

topics.each do |topic|
  puts "Topic #{topic[:topic_id]}: #{topic[:words].join(', ')}"
end

# Perform inference on new documents
new_document = "This is a new document"
tokenized_new_document = tokenizer.tokenize(new_document)
topic_distribution = model.infer(tokenized_new_document)

puts "Topic distribution for new document: #{topic_distribution}"

# Evaluate model coherence
coherence = model.coherence
puts "Model coherence: #{coherence}"
