#!/usr/bin/env ruby
# frozen_string_literal: true

#TODO: convert to class or module_function

@@word_buckets = {

  'document' => {
    attributes: ['title', 'content'],
    indexed: ['title'],
    collections: ['chunks'],
    references: ['topic', 'collection']
  },

  'chunk' => {
    attributes: ['text', 'tokenized_text', 'sanitized_text'],
    references: ['document', 'topic'],
    lists: ['vector_data']
  },

  'topic' => {
    attributes: ['name', 'description', 'vector'],
    indexed: ['name'],
    collections: ['documents', 'chunks'],
    references: ['collection']
  },

  'vector_data' => {
    attributes: ['vector'],
    references: ['chunk', 'topic']
  },

  'collection' => {
    attributes: ['name'],
    collections: ['documents', 'topics'],
    indexed: ['name']
  }

}
