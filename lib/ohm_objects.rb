require 'ohm'
require 'ohm/contrib'

# Collection object_bucket
class Collection < Ohm::Model
  attribute :name
  collection :documents, :Document
  collection :topics, :Topic
  unique :name
  index :name
end

# Document object_bucket
class Document < Ohm::Model
  attribute :title
  attribute :content
  attribute :processed, Ohm::DataTypes::Boolean
  collection :chunks, :Chunk
  reference :topic, :Topic
  reference :collection, :Collection  # Reference to the parent collection
  unique :title
  index :title
  index :processed

  def process!
    # Perform document processing here
    self.processed = true
    save
  end
end

# Chunk object_bucket
class Chunk < Ohm::Model
  attribute :text
  attribute :tokenized_text
  attribute :sanitized_text
  collection :words, :Word
  reference :document, :Document
  reference :topic, :Topic
  list :vector_data, :VectorData
end

# Word object_bucket
class Word < Ohm::Model
  attribute :word
  attribute :synsets
  attribute :part_of_speech
  attribute :named_entity
  reference :chunk, :Chunk
  collection :vector_data, :VectorData
end

# Topic object_bucket
class Topic < Ohm::Model
  attribute :name
  attribute :description
  attribute :vector
  collection :documents, :Document
  collection :chunks, :Chunk
  reference :collection, :Collection  # Reference to the parent collection
  unique :name
  index :name
end

# Vector Data object_bucket
class VectorData < Ohm::Model
  attribute :vector
  reference :chunk, :Chunk
  reference :topic, :Topic
end
