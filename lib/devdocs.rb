#!/usr/bin/env ruby
# frozen_string_literal: true

require "json"

DEVDOCS_ROOT = File.join("/srv/http/devdocs/public/docs")
DEVDOCS_INDEX = File.read(File.join(DEVDOCS_ROOT, 'docs.json'))

require 'pragmatic_tokenizer'

# Define a method to tokenize a document using pragmatic_tokenizer gem
def tokenize_document(document)
  tokenizer = PragmaticTokenizer::Tokenizer.new
  tokenizer.tokenize(document)
end

# Example usage
document = "This is a sample document."
tokens = tokenize_document(document)
puts tokens
# Output: ["This", "is", "a", "sample", "document."]


module KBGPT

  class DevDocs
    attr_reader :index

    def initialize
      @index = JSON.parse(DEVDOCS_INDEX)
    end

  end

end


def load_index_db(json)
  i = JSON.parse(File.read(json))
  return i
end


devdocs = KBGPT::DevDocs.new

# devdocs.index.each do |doc|
#   p doc["name"]
#   p doc["slug"]
#   doc_folder = File.join(DEVDOCS_ROOT, doc["slug"])
#   doc_index_db_file = File.join(doc_folder, "db.json")
#   doc_index = load_index_db(doc_index_db_file)
#   doc_index.each do |x|
#       p x
#       exit
#   end
#   exit
# end
