#!/usr/bin/env ruby
# frozen_string_literal: true

questions = JSON.load("questions.json")

# So, the idea is to have a question, right? So for example, question, what is a session manager in Linux audio and how does it help in organizing audio projects? Submit that to the API, get a response back. Take that question, send it through a rephrase function that somehow, so what is a session manager in Linux audio and how does it help in organizing audio projects? So I guess an example rephrase is, how could I better organize my studio sessions, is one. Or have a function that will take the generative grammars, the various grammars and apply certain rules to them that alters the question so it maintains the same structure but uses different keywords and then store these three questions and subsequent responses in a JSON-L file.
#

## arrange the questions from basic ---> complex




questions.each do |question|

  responses = {}

  response[0] = submit_query(question)

  response[1] = rephrase(question)

  response[2] = rephrase(question)

end
