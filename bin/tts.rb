#!/usr/bin/env ruby
# frozen_string_literal: true
#

require 'shellwords'
require 'rest-client'
require 'json'

words = ARGV[0]

data = {
  "model": "en-us-kathleen-low.onnx",
  "input": words
}

response = RestClient.post("http://localhost:8080/tts", data.to_json, content_type: :json)

if response.code == 200
  File.write('audio.wav', response.body)
  system('aplay audio.wav')
else
  puts "Error: Unable to generate audio"
end
