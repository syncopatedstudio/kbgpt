#!/usr/bin/env ruby
# frozen_string_literal: true

APP_ROOT = File.expand_path("../../", __FILE__)

lib_dir = File.expand_path(File.join(__dir__, '..', 'lib'))
$LOAD_PATH.unshift lib_dir unless $LOAD_PATH.include?(lib_dir)

require "dotenv/load"
require "json"
require "langchain"
require "ohm"
require "ohm/contrib"
require "redic"
require 'ruby/openai'

require "config"
require "util"
require "glob"

require "llm/openai"

require "db/cachestore"
require "db/vectorstore"

require "fileobject"
require "devdocs"

module KBGPT

  class Import
    attr_accessor :folders, :files

    def initialize(sources=[])
      files = []
      sources.each do |source|
        if File.directory?(source)
          files += Glob::documents(source)
        elsif File.file?(source)
          files << source
        else
          puts "neither file nor directory, exiting"
        end
      end
        @files = files.map { |file| KBGPT::FileObject.new(file) }
    end

  end

end

module KBGPT
  module_function

  def start

    if ARGV.any?
      sources = ARGV
    else
      sources = $config.fetch(:document)["folders"]
    end

    sources.each do |path|
      unless Dir.exist?(path)
        puts "#{path} not valid...exiting"; exit
      end
    end

    collection = KBGPT::Import.new(sources)
    return collection

    # pdf_objects = []
    # collection.files.each do |document|
    #   next unless document.type == ".pdf"
    #   pdf_objects << PDF2Text.new(document.path)
    # end

  end

end
